from django.contrib import admin

from .models import Product  # this is what called a relative import

admin.site.register(Product)
